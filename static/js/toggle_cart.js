//Корзина

$(document).ready(function(){

    $('#cart').on('click', function(e){
        e.preventDefault();
        $('.cart_toggled').toggleClass('hidden');
    })
    $('#cart').mouseover(function(){
        $('.cart_toggled').removeClass('hidden');
    })

});


//Фильтры
//Показать все
$(document).ready(function(){
    $('.all_products').on('click', function(e){
        e.preventDefault();
        $('div.filter_category').closest( ".filter_category_all" ).removeClass('hidden');
//        $('.filter_category_all').removeClass('hidden');
//        $('.filter_price').addClass('hidden');
//        $('.filter_category').addClass('hidden');
//        $('.filter_name').addClass('hidden');
//        $('#all_products').addClass('hidden');
//        $('.f').removeClass('hidden');
    })
});
$(document).ready(function(){

    $('.f_price').on('click', function(e){
        e.preventDefault();
        $('.filter_price').removeClass('hidden');
        $('.filter_category').addClass('hidden');
        $('.filter_name').addClass('hidden');

    })

    $('.f_name').on('click', function(e){
        e.preventDefault();
        $('.filter_name').closest('div').removeClass('hidden');
        $('.filter_category').addClass('hidden');
        $('.filter_price').addClass('hidden');

    })
});
$(document).ready(function() {
  // Custom
  var stickyToggle = function(sticky, stickyWrapper, scrollElement) {
    var stickyHeight = sticky.outerHeight();
    var stickyTop = stickyWrapper.offset().top;
    if (scrollElement.scrollTop() >= stickyTop){
      stickyWrapper.height(stickyHeight);
      sticky.addClass("is-sticky");
    }
    else{
      sticky.removeClass("is-sticky");
      stickyWrapper.height('auto');
    }
  };

  // Find all data-toggle="sticky-onscroll" elements
  $('[data-toggle="sticky-onscroll"]').each(function() {
    var sticky = $(this);
    var stickyWrapper = $('<div>').addClass('sticky-wrapper'); // insert hidden element to maintain actual top offset on page
    sticky.before(stickyWrapper);
    sticky.addClass('sticky');

    // Scroll & resize events
    $(window).on('scroll.sticky-onscroll resize.sticky-onscroll', function() {
      stickyToggle(sticky, stickyWrapper, $(this));
    });

    // On page load
    stickyToggle(sticky, stickyWrapper, $(window));
  });
});